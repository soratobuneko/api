Libera forms API
================

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://gitlab.com/liberaforms/api)


Project page [https://liberaforms.org](https://liberaforms.org)

We have built this software with the hope it will be used by our neighbours, friends, and anyone else who feels GAFAM already has way to much data on **all** of us.

Don't feed the Dictator!

Please read docs/INSTALL.txt for installation instructions.


## Getting started

```
python3 -m venv ./venv
source venv/bin/activate
pip install
flask run
```

An example of needed variables can be found in _.env.example_

## Migrations

We use [Alembic](https://github.com/sqlalchemy/alembic) for handling migrations

After making any changes to a Model in `api/models` a new
migration file needs to be created with:

```
flask db migrate
```

After you can execute the migration with:

```
flask db upgrade
```

## Docker

Build the image

```
docker build -t liberaforms-api:latest .
```

Run the image
```
docker run  --env-file ./.env liberaforms-api
```
