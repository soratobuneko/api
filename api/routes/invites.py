import datetime
from flask import request
from flask_jwt_extended import jwt_required, get_current_user
from flask_restful import Resource, reqparse, fields, marshal_with

from api.routes import rest
from api.routes.auth import user_has_roles
from api.models.invite import Invite as InviteModel
from .schemas.input.get_collection_query import size, page
from .schemas.output.list_meta import meta_fields

invite_fields = {
    'id': fields.String,
    'hostname': fields.String,
    'email': fields.String
}

invite_detail_fields = {
    'message': fields.String,
    'token': fields.Nested({
        'token': fields.String,
        'created': fields.String
    }),
    'admin': fields.Boolean
}

invite_detail_fields.update(invite_fields)

list_fields = {
    "items": fields.List(fields.Nested(invite_fields), attribute="items"),
    "meta": fields.Nested(meta_fields)
}

list_parser = reqparse.RequestParser()
list_parser.add_argument(**size)
list_parser.add_argument(**page)

class Invites(Resource):
    @user_has_roles(['admin'])
    @marshal_with(list_fields)
    def get(self):
        args = list_parser.parse_args()
        page = int(args.page)
        size = int(args.size)
        invites = InviteModel.query.paginate(page, size)
        return {
            'items': invites.items,
            'meta': {
                'count': invites.total
            }
        }, 200
    @user_has_roles(['admin'])
    @marshal_with(invite_detail_fields)
    def post(self):
        # TODO refactor Invite model so it doesnt use request
        # instead pass request args to create()
        # user = get_current_user()
        data = {
            # 'created': datetime.datetime.now().isoformat(),
            # 'author_id': user['_id']['$oid'],
            **request.json
        }
        invite = InviteModel.create(**data)
        return invite, 201


class Invite(Resource):
    @user_has_roles(['admin'])
    @marshal_with(invite_detail_fields)
    def get(self, id):
        invite = InviteModel.query.filter_by(id=id).first()
        return invite, 200
    @user_has_roles(['admin'])
    def put(self, id):
        update = {
            # TODO add updatedAt to site document
            # 'updatedAt': datetime.datetime.now().isoformat(),
            **request.json
        }
        # TODO this will break, old ORM
        InviteModel.objects(id=id).update(**update)
        return "OK", 204
    @user_has_roles(['admin'])
    def delete(self, id):
        # TODO this will break, old ORM
        invite = InviteModel.objects(id=id)
        if (invite):
            invite.delete()
            return "OK", 204
        return "Not found", 404


rest.add_resource(Invites, '/invites')
rest.add_resource(Invite, '/invites/<id>')
