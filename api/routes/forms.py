import datetime, uuid
from flask import request, jsonify
from flask_jwt_extended import jwt_required, decode_token, get_current_user
from flask_restful import Resource, reqparse, fields, marshal_with

from api.routes import rest
from api.models.form import Form as FormModel
from .schemas.input.get_collection_query import size, page
from .schemas.output.list_meta import meta_fields

form_fields = {
    'id': fields.String,
    'slug': fields.String,
    'createdAt': fields.String(attribute='created')
}

form_detail_fields = {
    'expired': fields.Boolean,
    'enabled': fields.Boolean
}

form_detail_fields.update(form_fields)

list_fields = {
    'items': fields.List(fields.Nested(form_fields), attribute='items'),
    'meta': fields.Nested(meta_fields)
}

list_parser = reqparse.RequestParser()
list_parser.add_argument(**size)
list_parser.add_argument(**page)

class Forms(Resource):
    @jwt_required
    @marshal_with(list_fields)
    def get(self):
        args = list_parser.parse_args()
        page = int(args.page)
        size = int(args.size)
        forms = FormModel.query.paginate(page, size)
        return {
            'items': forms.items,
            'meta': {
                'count': forms.total
            }
        }, 200
    @jwt_required
    @marshal_with(form_detail_fields)
    def post(self):
        user = get_current_user()
        data = {
            'created': datetime.datetime.now().isoformat(),
            'author_id': user['id'],
            'hostname': user['hostname'],
            'postalCode': '08014',
            'expired': False,
            "expiryConditions": {
                "totalEntries": 0,
                "expireDate": False,
                "fields": {}
            },
            'enabled': False,
            "sharedEntries": {
                "enabled": False,
                "key": uuid.uuid4().hex,
                "password": False,
                "expireDate": False
            },
            'introductionText': None,
            'consentTexts': None,
            'afterSubmitText': None,
            'expiredText': None,
            'sendConfirmation': False,
            'log': [],
            'restrictedAccess': False,
            'adminPreferences': { 'public': True },
            **request.json
        }
        form = FormModel.save_new_form(data)
        return form, 201


class Form(Resource):
    @jwt_required
    @marshal_with(form_detail_fields)
    def get(self, id):
        form = FormModel.objects.get(id=id)
        return form, 200
    @jwt_required
    def put(self, id):
        update = {
            # TODO add updatedAt to form document
            # 'updatedAt': datetime.datetime.now().isoformat(),
            **request.json
        }
        FormModel.objects(id=id).update(**update)
        return "OK", 204
    @jwt_required
    def delete(self, id):
        form = FormModel.objects(id=id)
        if (form):
            form.delete()
            return "OK", 204
        return "Not found", 404


rest.add_resource(Forms, '/forms')
rest.add_resource(Form, '/forms/<id>')
