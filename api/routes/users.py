from flask import jsonify
from flask_jwt_extended import jwt_required, get_current_user
from flask_restful import Resource, reqparse, fields, marshal_with, marshal

# TODO consider wrapping alchemy expcetions
from sqlalchemy.exc import IntegrityError

from api import db
from api.routes import rest
from api.models.user import User as UserModel
from api.routes.auth import user_has_roles
from .schemas.input.get_collection_query import size, page
from .schemas.input.fields import username, password, email, group
from .schemas.output.list_meta import meta_fields
from .schemas.output.error import error_fields

user_fields = {
    'id': fields.String,
    'username': fields.String
}

user_detail_fields = {
    'email': fields.String
}

user_detail_fields.update(user_fields)

list_fields = {
    "items": fields.List(fields.Nested(user_fields), attribute="items"),
    "meta": fields.Nested(meta_fields)
}

list_parser = reqparse.RequestParser()
list_parser.add_argument(**size)
list_parser.add_argument(**page)

create_parser = reqparse.RequestParser()
create_parser.add_argument(**email)
create_parser.add_argument(**username)
create_parser.add_argument(**password)
create_parser.add_argument(**group)

class Users(Resource):
    @user_has_roles(['admin'])
    @marshal_with(list_fields)
    def get(self):
        args = list_parser.parse_args()
        page = int(args.page)
        size = int(args.size)
        users = UserModel.query.paginate(page, size)
        return {
            'items': users.items,
            'meta': {
                'count': users.total
            }
        }, 200
    @user_has_roles(['admin'])
    def post(self):
        args = create_parser.parse_args()
        user = UserModel(
            username=args.username, 
            email=args.email,
            group=args.group
        )
        user.set_password(args.password)
        try: 
            db.session.add(user) 
            db.session.commit() 
            return {'id': user.id }, 201
        except IntegrityError as exception_message:
            return marshal({
                'message': exception_message
            }, error_fields), 400


class User(Resource):
    @user_has_roles(['admin'])
    @marshal_with(user_detail_fields)
    def get(self, id):
        user = UserModel.query.filter_by(id=id).first()
        return user, 200

class AuthUser(Resource):
    @jwt_required
    @marshal_with(user_detail_fields)
    def get(self):
        loggedUser = get_current_user()
        user = UserModel.query.filter_by(id=loggedUser['id']).first()
        return user, 200


rest.add_resource(Users, '/users')
rest.add_resource(AuthUser, '/users/me')
rest.add_resource(User, '/users/<id>')
