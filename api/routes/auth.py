from functools import wraps
from flask import jsonify
from flask_restful import Resource, reqparse
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_refresh_token_required,
    get_current_user,
    get_jwt_identity,
    verify_jwt_in_request
)

from api import jwt
from api.models.user import User
from api.routes import api_bp, rest
from api.routes.schemas.input.fields import username, password


def map_user(user):
    roles = user.group.strip().split(",")
    return {
        'id': str(user.id),
        'username': user.username,
        'email': user.email,
        'createdAt': user.createdAt,
        # 'validatedEmail': user.validatedEmail,
        # 'hostname': user.hostname,
        # 'preferences': user.preferences,
        'roles': roles
    }

@jwt.user_loader_callback_loader
def user_loader(identity):
    return identity


def user_has_roles(roles):
    def required_roles(fn):
        @wraps(fn)
        def wrapper(*args, **kwargs):
            verify_jwt_in_request()
            user = get_current_user()
            for role in roles:
                if role not in user['roles']:
                    return 'Forbidden', 403
            else:
                return fn(*args, **kwargs)

        return wrapper
    return required_roles

parser = reqparse.RequestParser()
parser.add_argument(**username)
parser.add_argument(**password)

DEFAULT_AUTH_ERROR_MSG = "Username or Password Error"

# Authentication resource
# post: login method to create access tokens and refresh tokens
class Authenitcation(Resource):
    def post(self):
        args = parser.parse_args()
        user = User.query.filter_by(username=args.username).first()
        if user and user.verify_password(args.password):
            identity = map_user(user)
            ret = {
                'access_token': create_access_token(identity=identity),
                'refresh_token': create_refresh_token(identity=identity)
            }
            return ret, 200
        else:
            return DEFAULT_AUTH_ERROR_MSG, 401


# The jwt_refresh_token_required decorator insures a valid refresh
# token is present in the request before calling this endpoint. We
# can use the get_jwt_identity() function to get the identity of
# the refresh token, and use the create_access_token() function again
# to make a new access token for this identity.
@api_bp.route('/auth/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    current_user = get_jwt_identity()
    ret = {'access_token': create_access_token(identity=current_user)}
    return jsonify(ret), 200


# Fresh login endpoint. This is designed to be used if we need to
# make a fresh token for a user (by verifying they have the
# correct username and password). Unlike the standard login endpoint,
# this will only return a new access token, so that we don't keep
# generating new refresh tokens, which entirely defeats their point.
# routes can be protected with @fresh_jwt_required decorator
@api_bp.route('/auth/fresh-login', methods=['POST'])
def fresh_login():
    args = parser.parse_args()
    user = User.query.filter_by(username=args.username).first()
    if user and user.verify_password(args.password):
        identity = map_user(user)
        new_token = create_access_token(identity=identity, fresh=True)
        ret = {'access_token': new_token}
        return jsonify(ret), 200
    else:
        return jsonify(DEFAULT_AUTH_ERROR_MSG), 401

# TODO
# Recover password endpoint
# @api_bp.route('/auth/recovery', methods=['POST'])
# def recover_password():
#     ret = {
#         'do': 'something'
#     }
#     return jsonify(ret), 200

rest.add_resource(Authenitcation, '/auth/login')
