import datetime
from flask import request
from flask_jwt_extended import jwt_required, get_current_user
from flask_restful import Resource, reqparse, fields, marshal_with

from liberaforms.api import rest
from liberaforms.models.site import Site as SiteModel
from .schemas.input.get_collection_query import size, offset
from .schemas.output.list_meta import meta_fields

site_fields = {
    'id': fields.String,
    'hostname': fields.String,
    'siteName': fields.String,
    'created': fields.String
}

site_detail_fields = {
    'scheme': fields.String,
    'defaultLanguage': fields.String,
    'blurb': fields.Nested({
      'html': fields.String,
      'markdown': fields.String,
    }),
    'invitationOnly': fields.Boolean
}

site_detail_fields.update(site_fields)

list_fields = {
    "items": fields.List(fields.Nested(site_fields), attribute="items"),
    "meta": fields.Nested(meta_fields)
}

list_parser = reqparse.RequestParser()
list_parser.add_argument(**size)
list_parser.add_argument(**offset)

class Sites(Resource):
    @jwt_required
    @marshal_with(list_fields)
    def get(self):
        args = list_parser.parse_args()
        offset = int(args.offset)
        limit = int(args.size)
        sites = SiteModel.objects().skip(offset).limit(limit)
        return {
            'items': sites,
            'meta': {
                'count': sites.count()
            }
        }, 200
    @jwt_required
    @marshal_with(site_detail_fields)
    def post(self):
        # TODO refactor Site model so it doesnt use request
        # instead pass request args to create()
        # user = get_current_user()
        # data = {
        #     'created': datetime.datetime.now().isoformat(),
        #     'author_id': user['_id']['$oid'],
        #     **request.json
        # }
        form = SiteModel.create()
        return form, 201


class Site(Resource):
    @jwt_required
    @marshal_with(site_detail_fields)
    def get(self, id):
        site = SiteModel.objects.get(id=id)
        return site, 200
    @jwt_required
    def put(self, id):
        update = {
            # TODO add updatedAt to site document
            # 'updatedAt': datetime.datetime.now().isoformat(),
            **request.json
        }
        SiteModel.objects(id=id).update(**update)
        return "OK", 204
    def delete(self, id):
        site = SiteModel.objects(id=id)
        if (site):
            site.delete()
            return "OK", 204
        return "Not found", 404


rest.add_resource(Sites, '/sites')
rest.add_resource(Site, '/sites/<id>')