## Response schemas
from flask_restful import fields

# Generic error schema
error_fields = {
    'message': fields.String
}
