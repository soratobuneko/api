## Response schemas
from flask_restful import fields

# Collections meta fields
meta_fields = {
    'count': fields.Integer
}