email = {
    "name": 'email',
    "dest": 'email',
    "location": 'json',
    "required": True,
    "help": 'The user\'s email'
}
username = {
    "name": 'username',
    "dest": 'username',
    "location": 'json',
    "required": True,
    "help": 'The user\'s username'
}
password = {
    "name": 'password',
    "dest": 'password',
    "location": 'json',
    "required": True,
    "help": 'The user\'s password'
}
group = {
    "name": 'group',
    "dest": 'group',
    "location": 'json',
    "required": True,
    "help": 'The user\'s group'
}
