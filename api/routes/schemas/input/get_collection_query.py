size = {
    "name": 'size', 
    "dest": 'size',
    "location": 'args',
    "default": 10
}

page = {
    "name": 'page', 
    "dest": 'page',
    "location": 'args',
    "default": 1
}