# Translations template for PROJECT.
# Copyright (C) 2020 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2020-11-02 20:52+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.3.4\n"

#: models/form.py:118
msgid "Marked"
msgstr ""

#: models/form.py:119 templates/admin/inspect-user.html:39
#: templates/admin/inspect-user.html:93 templates/admin/list-forms.html:28
#: templates/admin/list-users.html:29 templates/form/inspect-form.html:210
#: templates/form/my-forms.html:35 templates/site/admin-panel.html:28
#: templates/site/admin-panel.html:147 templates/site/admin-panel.html:190
msgid "Created"
msgstr ""

#: models/form.py:123 utils/form_helper.py:63
msgid "Label"
msgstr ""

#: models/form.py:164 templates/form/inspect-form.html:151
#: templates/site/admin-panel.html:97 utils/consent_texts.py:136
msgid "DPL"
msgstr ""

#: models/form.py:314
msgid "Sorry, this form has expired."
msgstr ""

#: models/form.py:342
msgid "Thank you!!"
msgstr ""

#: models/form.py:689
msgid "Form title"
msgstr ""

#: models/form.py:690
msgid "Context"
msgstr ""

#: models/form.py:691
msgid ""
" * Describe your form.\n"
" * Add relevant content, links, images, etc."
msgstr ""

#: models/site.py:361
msgid ""
"Hello,\n"
"\n"
"You have been invited to LiberaForms.\n"
"\n"
"Regards."
msgstr ""

#: templates/base.html:43 templates/site/admin-panel.html:138
msgid "Toggle root mode"
msgstr ""

#: templates/admin/delete-user.html:21 templates/admin/inspect-user.html:86
#: templates/admin/inspect-user.html:199 templates/admin/list-users.html:32
#: templates/base.html:55 templates/site/admin-panel.html:149
#: templates/site/edit-site.html:26
msgid "Forms"
msgstr ""

#: templates/admin/list-users.html:12 templates/base.html:58
#: templates/site/admin-panel.html:148 templates/site/edit-site.html:22
msgid "Users"
msgstr ""

#: templates/base.html:62 templates/form/my-forms.html:13
msgid "My forms"
msgstr ""

#: templates/base.html:74
msgid "Config"
msgstr ""

#: templates/base.html:82
msgid "Profile"
msgstr ""

#: templates/base.html:88
msgid "Logout"
msgstr ""

#: templates/base.html:102 templates/base.html:106 templates/user/login.html:10
#: templates/user/login.html:23
msgid "Login"
msgstr ""

#: templates/base.html:109
msgid "Create an account"
msgstr ""

#: templates/admin/change-author.html:10 templates/form/delete-form.html:12
#: templates/form/edit-form.html:17 templates/form/expiration.html:14
#: templates/form/list-log.html:14 templates/form/share-form.html:10
msgid "Return to form"
msgstr ""

#: templates/admin/change-author.html:13 templates/admin/change-author.html:39
#: templates/form/inspect-form.html:297
msgid "Change author"
msgstr ""

#: templates/admin/change-author.html:18
msgid "Form site"
msgstr ""

#: templates/admin/change-author.html:23
msgid "Current author"
msgstr ""

#: templates/admin/change-author.html:28
msgid "You are going to change the author of this form."
msgstr ""

#: templates/admin/change-author.html:29
msgid "Are you sure?"
msgstr ""

#: templates/admin/change-author.html:34
msgid "Current author's username"
msgstr ""

#: templates/admin/change-author.html:36
msgid "New author's username"
msgstr ""

#: templates/admin/change-author.html:46 templates/admin/delete-user.html:26
#: templates/admin/inspect-user.html:95 templates/form/share-form.html:19
msgid "Editors"
msgstr ""

#: templates/admin/change-author.html:55 templates/admin/inspect-user.html:94
#: templates/admin/list-forms.html:29 templates/form/inspect-form.html:196
#: templates/form/share-form.html:31
msgid "Author"
msgstr ""

#: templates/admin/change-author.html:57
msgid "Editor"
msgstr ""

#: templates/admin/delete-user.html:12 templates/admin/delete-user.html:54
#: templates/admin/inspect-user.html:73
msgid "Delete user"
msgstr ""

#: templates/admin/delete-user.html:14
msgid "You are going to delete a user"
msgstr ""

#: templates/admin/delete-user.html:16
msgid "and their forms"
msgstr ""

#: templates/admin/delete-user.html:25 templates/admin/inspect-user.html:92
#: templates/admin/list-forms.html:25 templates/form/inspect-form.html:40
#: templates/form/my-forms.html:32
msgid "Name"
msgstr ""

#: templates/admin/delete-user.html:27 templates/admin/inspect-user.html:97
#: templates/admin/inspect-user.html:191 templates/admin/list-forms.html:26
#: templates/form/my-forms.html:33 templates/site/stats.html:44
#: templates/site/stats.html:120 templates/user/statistics.html:31
msgid "Entries"
msgstr ""

#: templates/admin/delete-user.html:40
msgid "Write the name of the user below"
msgstr ""

#: templates/admin/delete-user.html:52
msgid "Delete user and forms"
msgstr ""

#: templates/admin/delete-user.html:56 templates/common/change-language.html:41
#: templates/common/change-language.html:43
#: templates/entries/delete-entries.html:29
#: templates/entries/list-entries.html:244 templates/form/edit-form.html:83
#: templates/form/edit-form.html:85 templates/form/expiration.html:145
#: templates/form/inspect-form.html:172 templates/form/inspect-form.html:240
#: templates/form/inspect-form.html:266 templates/form/preview-form.html:33
#: templates/form/preview-form.html:36 templates/main/index.html:28
#: templates/site/change-site-favicon.html:28
#: templates/site/change-sitename.html:23 templates/site/consent.html:76
#: templates/site/delete-site.html:37 templates/site/menu-color.html:67
#: templates/site/new-invite.html:61 templates/site/smtp-config.html:48
#: templates/user/change-email.html:24 templates/user/consent.html:61
#: templates/user/new-user.html:92 templates/user/recover-password.html:25
msgid "Cancel"
msgstr ""

#: templates/admin/inspect-user.html:16
msgid "User details"
msgstr ""

#: templates/admin/inspect-user.html:20 templates/admin/list-users.html:23
#: utils/wtf.py:35 utils/wtf.py:72
msgid "Username"
msgstr ""

#: templates/admin/inspect-user.html:25 templates/admin/list-forms.html:33
#: templates/admin/list-users.html:26 templates/form/inspect-form.html:215
#: templates/site/edit-site.html:14
msgid "Hostname"
msgstr ""

#: templates/admin/inspect-user.html:30
msgid "Is Admin"
msgstr ""

#: templates/admin/inspect-user.html:33 templates/admin/inspect-user.html:53
#: templates/admin/inspect-user.html:63 templates/admin/inspect-user.html:107
#: templates/entries/list-entries.html:73
#: templates/entries/list-entries.html:121 templates/form/expiration.html:21
#: templates/form/inspect-form.html:77 templates/form/inspect-form.html:92
#: templates/form/inspect-form.html:100 templates/form/inspect-form.html:112
#: templates/form/inspect-form.html:126 templates/site/admin-panel.html:88
#: templates/user/user-settings.html:72 templates/user/user-settings.html:97
#: templates/user/user-settings.html:106
msgid "True"
msgstr ""

#: templates/admin/inspect-user.html:34 templates/admin/inspect-user.html:55
#: templates/admin/inspect-user.html:64 templates/admin/inspect-user.html:109
#: templates/entries/list-entries.html:74
#: templates/entries/list-entries.html:123 templates/form/expiration.html:22
#: templates/form/inspect-form.html:78 templates/form/inspect-form.html:94
#: templates/form/inspect-form.html:102 templates/form/inspect-form.html:114
#: templates/form/inspect-form.html:128 templates/site/admin-panel.html:89
#: templates/user/user-settings.html:73 templates/user/user-settings.html:98
#: templates/user/user-settings.html:107
msgid "False"
msgstr ""

#: templates/admin/inspect-user.html:43
msgid "Language"
msgstr ""

#: templates/admin/inspect-user.html:47 templates/admin/list-users.html:31
#: templates/site/admin-panel.html:29 utils/wtf.py:36
msgid "Email"
msgstr ""

#: templates/admin/inspect-user.html:51
msgid "Validated email"
msgstr ""

#: templates/admin/inspect-user.html:60
msgid "Blocked by admin"
msgstr ""

#: templates/admin/inspect-user.html:71
msgid "Delete user and authored forms"
msgstr ""

#: templates/admin/inspect-user.html:81 templates/admin/list-forms.html:17
#: templates/admin/list-users.html:15 templates/form/my-forms.html:20
msgid "Statistics"
msgstr ""

#: templates/admin/inspect-user.html:96 templates/admin/list-forms.html:30
#: templates/form/inspect-form.html:73 templates/form/my-forms.html:36
msgid "Public"
msgstr ""

#: templates/admin/list-forms.html:14
msgid "All forms"
msgstr ""

#: templates/admin/list-forms.html:27 templates/form/inspect-form.html:44
#: templates/form/my-forms.html:34
msgid "Last entry"
msgstr ""

#: templates/admin/list-users.html:30 templates/form/inspect-form.html:141
#: templates/form/inspect-form.html:155 templates/form/share-form.html:61
#: templates/user/consent.html:29
msgid "Enabled"
msgstr ""

#: templates/admin/list-users.html:33 templates/site/admin-panel.html:33
msgid "Admin"
msgstr ""

#: templates/common/change-language.html:11
msgid "Change language"
msgstr ""

#: templates/common/change-language.html:39
#: templates/entries/list-entries.html:243 templates/form/edit-form.html:82
#: templates/form/expiration.html:144 templates/form/inspect-form.html:171
#: templates/form/inspect-form.html:239 templates/form/inspect-form.html:265
#: templates/form/preview-form.html:30 templates/main/index.html:27
#: templates/site/consent.html:75 templates/site/menu-color.html:66
#: templates/user/change-email.html:23 templates/user/consent.html:60
msgid "Save"
msgstr ""

#: templates/entries/delete-entries.html:12
#: templates/entries/delete-entries.html:28
msgid "Delete entries"
msgstr ""

#: templates/entries/delete-entries.html:13
#: templates/form/inspect-form.html:309 views/form.py:153
msgid "Form"
msgstr ""

#: templates/entries/delete-entries.html:15
msgid "You are going to delete"
msgstr ""

#: templates/entries/delete-entries.html:16 templates/form/delete-form.html:21
#: templates/site/delete-site.html:22
msgid "entries"
msgstr ""

#: templates/entries/delete-entries.html:19
msgid "Write the total number of entries below"
msgstr ""

#: templates/entries/list-entries.html:21
msgid "Form details"
msgstr ""

#: templates/entries/list-entries.html:27
#: templates/entries/view-results.html:20 templates/form/inspect-form.html:28
msgid "Graphs"
msgstr ""

#: templates/entries/list-entries.html:31
#: templates/entries/list-entries.html:35
#: templates/entries/view-results.html:23
msgid "Download CSV"
msgstr ""

#: templates/entries/list-entries.html:42
msgid "Disable edition"
msgstr ""

#: templates/entries/list-entries.html:46
msgid "Enable edition"
msgstr ""

#: templates/entries/list-entries.html:52
msgid "Hide deleted columns"
msgstr ""

#: templates/entries/list-entries.html:56
msgid "Show deleted columns"
msgstr ""

#: templates/entries/list-entries.html:61
msgid "Delete all"
msgstr ""

#: templates/entries/list-entries.html:76
msgid "Notify me on each new entry"
msgstr ""

#: templates/entries/list-entries.html:115
#: templates/entries/list-entries.html:117
msgid "Mark"
msgstr ""

#: templates/entries/list-entries.html:201
msgid "Delete entry"
msgstr ""

#: templates/form/delete-form.html:17 templates/form/inspect-form.html:286
msgid "Delete form"
msgstr ""

#: templates/form/delete-form.html:20
msgid "You are going to delete this form and"
msgstr ""

#: templates/form/delete-form.html:23
msgid "Write the name of the form below"
msgstr ""

#: templates/form/delete-form.html:33 templates/site/admin-panel.html:47
#: templates/site/delete-site.html:36 templates/site/edit-site.html:64
msgid "Delete"
msgstr ""

#: templates/form/edit-form.html:21
msgid "Build your form here"
msgstr ""

#: templates/form/edit-form.html:31
msgid "Introduction text"
msgstr ""

#: templates/form/edit-form.html:34
msgid "This is a markdown editor, an easy way to write HTML."
msgstr ""

#: templates/form/edit-form.html:35
msgid "Learn more"
msgstr ""

#: templates/form/edit-form.html:44
msgid "The form"
msgstr ""

#: templates/form/edit-form.html:61
msgid "Your form will have this address"
msgstr ""

#: templates/form/edit-form.html:75
msgid "This URL is not available. Try another!"
msgstr ""

#: templates/form/edit-form.html:80 templates/form/edit-form.html:168
#: templates/form/preview-form.html:12
msgid "Preview"
msgstr ""

#: templates/form/edit-form.html:153
msgid "OK, use this URL"
msgstr ""

#: templates/form/edit-form.html:193
msgid "Cannot modify because a response with this value exists in the database"
msgstr ""

#: templates/form/expiration.html:24
msgid "Notify me when this form expires"
msgstr ""

#: templates/form/expiration.html:29 templates/form/inspect-form.html:90
#: templates/form/inspect-form.html:98
msgid "Expired"
msgstr ""

#: templates/form/expiration.html:36
msgid "Expiration conditions"
msgstr ""

#: templates/form/expiration.html:40
msgid "Date / Time"
msgstr ""

#: templates/form/expiration.html:57 templates/site/smtp-config.html:47
msgid "Save changes"
msgstr ""

#: templates/form/expiration.html:69
msgid "Fields"
msgstr ""

#: templates/form/expiration.html:86
msgid "Save max."
msgstr ""

#: templates/form/expiration.html:99
msgid "Maximum responses"
msgstr ""

#: templates/form/expiration.html:104
msgid "The form will expire after"
msgstr ""

#: templates/form/expiration.html:118
msgid "responses"
msgstr ""

#: templates/form/expiration.html:130 templates/form/inspect-form.html:248
msgid "When the form has expired"
msgstr ""

#: templates/form/expiration.html:133 templates/form/inspect-form.html:159
#: templates/form/inspect-form.html:226 templates/form/inspect-form.html:252
#: templates/main/index.html:21
msgid "Edit text"
msgstr ""

#: templates/form/expiration.html:138 templates/form/inspect-form.html:258
msgid "Say something to the user if the form has expired"
msgstr ""

#: templates/form/form_templates.html:11
msgid "Form templates"
msgstr ""

#: templates/form/form_templates.html:14
msgid "We've put together some forms you can use to get started."
msgstr ""

#: templates/form/form_templates.html:16
msgid "Choose the template that best suits your needs and edit it to fit."
msgstr ""

#: templates/form/form_templates.html:33
msgid "Use this template"
msgstr ""

#: templates/form/inspect-form.html:20
msgid "Details"
msgstr ""

#: templates/form/inspect-form.html:24
msgid "View entries"
msgstr ""

#: templates/form/inspect-form.html:32 templates/site/stats.html:51
#: templates/site/stats.html:127 templates/user/statistics.html:40
msgid "Total entries"
msgstr ""

#: templates/form/inspect-form.html:57
msgid "Public URL"
msgstr ""

#: templates/form/inspect-form.html:59 templates/form/inspect-form.html:142
#: templates/form/inspect-form.html:156 templates/form/inspect-form.html:198
#: templates/form/share-form.html:62 templates/user/consent.html:30
msgid "Disabled"
msgstr ""

#: templates/form/inspect-form.html:86
msgid "Expiration"
msgstr ""

#: templates/form/inspect-form.html:108 templates/form/my-forms.html:37
#: templates/site/consent.html:38
msgid "Shared"
msgstr ""

#: templates/form/inspect-form.html:122 templates/form/share-form.html:92
msgid "Restricted access"
msgstr ""

#: templates/form/inspect-form.html:137
msgid "Confirmation"
msgstr ""

#: templates/form/inspect-form.html:169 templates/site/consent.html:30
#: templates/user/consent.html:22
msgid "Data protection"
msgstr ""

#: templates/form/inspect-form.html:175
msgid "Use the template"
msgstr ""

#: templates/form/inspect-form.html:182 templates/form/inspect-form.html:516
#: templates/form/inspect-form.html:555 templates/site/consent.html:64
#: templates/site/consent.html:83 templates/user/consent.html:48
#: templates/user/consent.html:67 templates/user/new-user.html:65
#: utils/consent_texts.py:71
msgid "I agree"
msgstr ""

#: templates/form/inspect-form.html:222
msgid "After replying to the form"
msgstr ""

#: templates/form/inspect-form.html:232
msgid "Say something to the user after the form has been submitted"
msgstr ""

#: templates/form/inspect-form.html:275
msgid "Other options"
msgstr ""

#: templates/form/inspect-form.html:281
msgid "View log"
msgstr ""

#: templates/form/inspect-form.html:282
msgid "Duplicate form"
msgstr ""

#: templates/form/inspect-form.html:283
msgid "Embed form"
msgstr ""

#: templates/form/inspect-form.html:288
msgid "Delete form and entries"
msgstr ""

#: templates/form/inspect-form.html:294
msgid "Admin options"
msgstr ""

#: templates/form/inspect-form.html:296
msgid "Duplicate"
msgstr ""

#: templates/form/inspect-form.html:299
msgid "Disable"
msgstr ""

#: templates/form/inspect-form.html:301
msgid "Enable"
msgstr ""

#: templates/form/inspect-form.html:313
msgid "Edit form"
msgstr ""

#: templates/form/inspect-form.html:323 templates/form/preview-form.html:19
#: templates/form/view-form.html:22
msgid "Required fields"
msgstr ""

#: templates/form/inspect-form.html:329 templates/form/view-form.html:32
msgid "Send me confirmation by mail"
msgstr ""

#: templates/form/inspect-form.html:343 templates/form/preview-form.html:23
#: templates/form/view-form.html:48 templates/site/smtp-config.html:61
msgid "Send"
msgstr ""

#: templates/form/list-log.html:18
msgid "Log"
msgstr ""

#: templates/form/list-log.html:22
msgid "Time"
msgstr ""

#: templates/form/list-log.html:23 utils/wtf.py:109
msgid "User"
msgstr ""

#: templates/form/list-log.html:24
msgid "Message"
msgstr ""

#: templates/form/my-forms.html:17
msgid "New form"
msgstr ""

#: templates/form/preview-form.html:32 templates/form/preview-form.html:35
msgid "Edit again"
msgstr ""

#: templates/form/share-form.html:20
msgid "Users listed here have the <b>same permissions</b> as you"
msgstr ""

#: templates/form/share-form.html:34
msgid "Remove"
msgstr ""

#: templates/form/share-form.html:46
msgid "Add new editor"
msgstr ""

#: templates/form/share-form.html:57
msgid "Share results"
msgstr ""

#: templates/form/share-form.html:58
msgid "Readonly access for <b>anyone</b> with a copy of these links"
msgstr ""

#: templates/form/share-form.html:93
msgid "Only visible to people with a user account (login required)"
msgstr ""

#: templates/form/share-form.html:96
msgid "Restricted"
msgstr ""

#: templates/form/share-form.html:97
msgid "Public form"
msgstr ""

#: templates/main/page-not-found.html:7
msgid "Opps! 404"
msgstr ""

#: templates/main/server-error.html:7
msgid "Opps! 500 server error"
msgstr ""

#: templates/site/admin-panel.html:11
msgid "Administrator"
msgstr ""

#: templates/site/admin-panel.html:16
msgid "Invitations"
msgstr ""

#: templates/site/admin-panel.html:20
msgid "Invite a new user"
msgstr ""

#: templates/site/admin-panel.html:53
msgid "Invitation message"
msgstr ""

#: templates/site/admin-panel.html:63
msgid "Site settings"
msgstr ""

#: templates/site/admin-panel.html:68
msgid "Site name"
msgstr ""

#: templates/site/admin-panel.html:73 templates/site/admin-panel.html:80
#: templates/site/admin-panel.html:109 templates/site/admin-panel.html:116
#: templates/site/edit-site.html:59 templates/user/user-settings.html:34
#: templates/user/user-settings.html:48 templates/user/user-settings.html:56
msgid "Change"
msgstr ""

#: templates/site/admin-panel.html:77
msgid "Default language"
msgstr ""

#: templates/site/admin-panel.html:84 templates/site/edit-site.html:30
msgid "Invitation only"
msgstr ""

#: templates/site/admin-panel.html:94
msgid "Consent"
msgstr ""

#: templates/site/admin-panel.html:96 templates/site/consent.html:28
#: templates/user/new-user.html:50
msgid "Terms and conditions"
msgstr ""

#: templates/site/admin-panel.html:97
msgid "etc"
msgstr ""

#: templates/site/admin-panel.html:100 templates/site/admin-panel.html:167
#: templates/site/consent.html:35 templates/user/consent.html:33
#: templates/user/user-settings.html:82
msgid "Edit"
msgstr ""

#: templates/site/admin-panel.html:104
msgid "Favicon"
msgstr ""

#: templates/site/admin-panel.html:113 templates/site/menu-color.html:9
msgid "Menu color"
msgstr ""

#: templates/site/admin-panel.html:120 utils/wtf.py:104
msgid "Email server"
msgstr ""

#: templates/site/admin-panel.html:123
msgid "Configure"
msgstr ""

#: templates/site/admin-panel.html:130
msgid "Root user"
msgstr ""

#: templates/site/admin-panel.html:184
msgid "Installation"
msgstr ""

#: templates/site/change-site-favicon.html:12
msgid "Change site favicon"
msgstr ""

#: templates/site/change-site-favicon.html:16
msgid "Upload image"
msgstr ""

#: templates/site/change-site-favicon.html:21
msgid "A small, square PNG image"
msgstr ""

#: templates/site/change-site-favicon.html:26
msgid "Upload"
msgstr ""

#: templates/site/change-site-favicon.html:27
msgid "Use default"
msgstr ""

#: templates/site/change-sitename.html:11
msgid "Change site name"
msgstr ""

#: templates/site/change-sitename.html:13
msgid "New site name"
msgstr ""

#: templates/site/change-sitename.html:22 templates/user/new-user.html:88
msgid "Submit"
msgstr ""

#: templates/site/consent.html:14
msgid "Default texts"
msgstr ""

#: templates/site/consent.html:17
msgid "Preview New user form"
msgstr ""

#: templates/site/consent.html:39
msgid "No"
msgstr ""

#: templates/site/consent.html:44
msgid "Add to New user form"
msgstr ""

#: templates/site/consent.html:52
msgid "New users must agree to these terms and conditions"
msgstr ""

#: templates/site/consent.html:88 templates/user/consent.html:72
msgid "Required"
msgstr ""

#: templates/site/consent.html:96 templates/user/user-settings.html:79
msgid "Consentment texts"
msgstr ""

#: templates/site/consent.html:99
msgid "Shared texts can be used by other users in their forms as templates"
msgstr ""

#: templates/site/delete-site.html:11
msgid "Delete site"
msgstr ""

#: templates/site/delete-site.html:13
msgid "You are going to delete this Site"
msgstr ""

#: templates/site/delete-site.html:16
msgid "Including"
msgstr ""

#: templates/site/delete-site.html:18
msgid "users"
msgstr ""

#: templates/site/delete-site.html:20
msgid "forms"
msgstr ""

#: templates/site/delete-site.html:25
msgid "Write the name of the site below"
msgstr ""

#: templates/site/edit-site.html:18
msgid "Sitename"
msgstr ""

#: templates/site/edit-site.html:35
msgid "Public link creation"
msgstr ""

#: templates/site/edit-site.html:43
msgid "Scheme"
msgstr ""

#: templates/site/edit-site.html:65 templates/user/new-user.html:90
msgid "Go back"
msgstr ""

#: templates/site/new-invite.html:11
msgid "Send a new user invitation"
msgstr ""

#: templates/site/new-invite.html:25
msgid "The link to the invitation will be appended to this message"
msgstr ""

#: templates/site/new-invite.html:35
msgid "Root user options"
msgstr ""

#: templates/site/new-invite.html:37
msgid "Invite to one of these sites"
msgstr ""

#: templates/site/new-invite.html:60
msgid "Send email"
msgstr ""

#: templates/site/smtp-config.html:9
msgid "Email server configuration"
msgstr ""

#: templates/site/smtp-config.html:54
msgid "Test configuration"
msgstr ""

#: templates/site/stats.html:12
msgid "Site statistics"
msgstr ""

#: templates/site/stats.html:59 templates/site/stats.html:135
#: templates/user/statistics.html:48
msgid "New forms"
msgstr ""

#: templates/site/stats.html:66 templates/site/stats.html:142
#: templates/user/statistics.html:55
msgid "Total forms"
msgstr ""

#: templates/site/stats.html:75 templates/site/stats.html:151
msgid "New users"
msgstr ""

#: templates/site/stats.html:83 templates/site/stats.html:159
msgid "Total users"
msgstr ""

#: templates/user/change-email.html:9
msgid "Change email address"
msgstr ""

#: templates/user/consent.html:14
msgid "Default consent text"
msgstr ""

#: templates/user/consent.html:80
msgid "Users may include the following texts in their forms"
msgstr ""

#: templates/user/login.html:24
msgid "Forgot your password?"
msgstr ""

#: templates/user/new-user.html:11
msgid "New user"
msgstr ""

#: templates/user/new-user.html:17
msgid "Letters and numbers. No accents."
msgstr ""

#: templates/user/new-user.html:32 templates/user/reset-password.html:16
msgid "Use a passphrase, with spaces"
msgstr ""

#: templates/user/new-user.html:33 templates/user/reset-password.html:17
msgid "eg: No holidays this year"
msgstr ""

#: templates/user/new-user.html:40 templates/user/reset-password.html:24
msgid "Another eg: My dog has fleas"
msgstr ""

#: templates/user/new-user.html:105
msgid "We sent you an email!"
msgstr ""

#: templates/user/recover-password.html:11
msgid "Recover password"
msgstr ""

#: templates/user/recover-password.html:24
msgid "Recover"
msgstr ""

#: templates/user/reset-password.html:10
msgid "Change password"
msgstr ""

#: templates/user/reset-password.html:30
msgid "Reset password"
msgstr ""

#: templates/user/statistics.html:12
msgid "My statistics"
msgstr ""

#: templates/user/user-settings.html:10
msgid "Hello"
msgstr ""

#: templates/user/user-settings.html:16
msgid "Your email has not been validated"
msgstr ""

#: templates/user/user-settings.html:20
msgid "Validate my email"
msgstr ""

#: templates/user/user-settings.html:29 utils/wtf.py:80
msgid "Email address"
msgstr ""

#: templates/user/user-settings.html:38 utils/wtf.py:37 utils/wtf.py:73
#: utils/wtf.py:95 utils/wtf.py:110
msgid "Password"
msgstr ""

#: templates/user/user-settings.html:53
msgid "Preferred language"
msgstr ""

#: templates/user/user-settings.html:63
msgid "By default"
msgstr ""

#: templates/user/user-settings.html:68
msgid "New entry notification default"
msgstr ""

#: templates/user/user-settings.html:89
msgid "My admin settings"
msgstr ""

#: templates/user/user-settings.html:94
msgid "Notify me when new user has registered"
msgstr ""

#: templates/user/user-settings.html:103
msgid "Notify me when new form has been created"
msgstr ""

#: utils/consent_texts.py:125
msgid "Please accept our terms and conditions."
msgstr ""

#: utils/consent_texts.py:137
msgid ""
"We take your data protection seriously. Please contact us for any "
"inquiries."
msgstr ""

#: utils/email.py:87
msgid "Congratulations!"
msgstr ""

#: utils/email.py:88
msgid "SMTP test"
msgstr ""

#: utils/email.py:98
#, python-format
msgid "Invitation to %s"
msgstr ""

#: utils/email.py:119
#, python-format
msgid "New user '%s' created at %s"
msgstr ""

#: utils/email.py:120
msgid "LiberaForms. New user notification"
msgstr ""

#: utils/email.py:130
msgid "Please use this link to recover your password"
msgstr ""

#: utils/email.py:132
msgid "LiberaForms. Recover password"
msgstr ""

#: utils/email.py:140
#, python-format
msgid ""
"Hello %s\n"
"\n"
"Please confirm your email\n"
"\n"
"%s"
msgstr ""

#: utils/email.py:142
msgid "LiberaForms. Confirm email"
msgstr ""

#: utils/email.py:164
#, python-format
msgid "New form '%s' created at %s"
msgstr ""

#: utils/email.py:165
msgid "LiberaForms. New form notification"
msgstr ""

#: utils/email.py:174
#, python-format
msgid "New form entry in %s at %s\n"
msgstr ""

#: utils/email.py:178
msgid "LiberaForms. New form entry"
msgstr ""

#: utils/email.py:190
msgid "Confirmation message"
msgstr ""

#: utils/email.py:197
#, python-format
msgid "The form '%s' has expired at %s"
msgstr ""

#: utils/email.py:198
msgid "LiberaForms. A form has expired"
msgstr ""

#: utils/wraps.py:108
msgid "That's a nasty key!"
msgstr ""

#: utils/wraps.py:119
msgid "That's a nasty token!"
msgstr ""

#: utils/wtf.py:38 utils/wtf.py:96
msgid "Password again"
msgstr ""

#: utils/wtf.py:44
msgid "Username is not valid"
msgstr ""

#: utils/wtf.py:47 utils/wtf.py:50
msgid "Please use a different username"
msgstr ""

#: utils/wtf.py:54 utils/wtf.py:57 utils/wtf.py:88 utils/wtf.py:91
#: utils/wtf.py:122 utils/wtf.py:125
msgid "Please use a different email address"
msgstr ""

#: utils/wtf.py:61 utils/wtf.py:100
msgid "Your password is weak"
msgstr ""

#: utils/wtf.py:65
msgid "Please accept our terms and conditions"
msgstr ""

#: utils/wtf.py:69
msgid "Please accept our data protection policy"
msgstr ""

#: utils/wtf.py:84
msgid "New email address"
msgstr ""

#: utils/wtf.py:105
msgid "Port"
msgstr ""

#: utils/wtf.py:106
msgid "Encryption"
msgstr ""

#: utils/wtf.py:111
msgid "Sender address"
msgstr ""

#: utils/wtf.py:115
msgid "New user's email"
msgstr ""

#: utils/wtf.py:116
msgid "Include message"
msgstr ""

#: utils/wtf.py:117
msgid "Make the new user an Admin"
msgstr ""

#: utils/wtf.py:118
msgid "hostname"
msgstr ""

#: utils/wtf.py:129
msgid "HTML color code"
msgstr ""

#: utils/wtf.py:132
msgid "Not a valid HTML color code"
msgstr ""

#: views/admin.py:64 views/admin.py:102
msgid "User not found"
msgstr ""

#: views/admin.py:107
msgid "Cannot delete root user"
msgstr ""

#: views/admin.py:110
msgid "Cannot delete yourself"
msgstr ""

#: views/admin.py:114
#, python-format
msgid "Deleted user '%s'"
msgstr ""

#: views/admin.py:117
msgid "Username does not match"
msgstr ""

#: views/admin.py:135 views/admin.py:146 views/entries.py:42
#: views/entries.py:55 views/entries.py:65 views/entries.py:149
#: views/form.py:133 views/form.py:276 views/form.py:294 views/form.py:309
#: views/form.py:319 views/form.py:354 views/form.py:429 views/form.py:444
#: views/form.py:542
msgid "Can't find that form"
msgstr ""

#: views/admin.py:152
msgid "Current author incorrect"
msgstr ""

#: views/admin.py:160
#, python-format
msgid "Changed author from %s to %s"
msgstr ""

#: views/admin.py:161
msgid "Changed author OK"
msgstr ""

#: views/admin.py:164
#, python-format
msgid "Cannot use %s. The user is not enabled"
msgstr ""

#: views/admin.py:166
#, python-format
msgid "Can't find username %s"
msgstr ""

#: views/entries.py:89
msgid "Deleted an entry"
msgstr ""

#: views/entries.py:105
msgid "Undeleted an entry"
msgstr ""

#: views/entries.py:140
msgid "Modified an entry"
msgstr ""

#: views/entries.py:155
msgid "We expected a number"
msgstr ""

#: views/entries.py:162
#, python-format
msgid "Deleted %s entries"
msgstr ""

#: views/entries.py:165
msgid "Number of entries does not match"
msgstr ""

#: views/form.py:73
msgid "Something went wrong. id does not match session['form_id']"
msgstr ""

#: views/form.py:77
msgid "You can't edit that form"
msgstr ""

#: views/form.py:85
msgid "Something went wrong. No slug!"
msgstr ""

#: views/form.py:165
msgid "Updated form OK"
msgstr ""

#: views/form.py:166
msgid "Form edited"
msgstr ""

#: views/form.py:172
msgid "Slug is missing."
msgstr ""

#: views/form.py:175
#, python-format
msgid "Slug is not unique. %s"
msgstr ""

#: views/form.py:215
msgid "Form created"
msgstr ""

#: views/form.py:216
msgid "Saved form OK"
msgstr ""

#: views/form.py:233 views/form.py:242 views/form.py:255 views/form.py:268
#: views/site.py:105
msgid "An error occured"
msgstr ""

#: views/form.py:282
#, python-format
msgid "Deleted '%s' and %s entries"
msgstr ""

#: views/form.py:285
msgid "Form name does not match"
msgstr ""

#: views/form.py:298
msgid "Permission needed to view form"
msgstr ""

#: views/form.py:325
msgid "Can't find a user with that email"
msgstr ""

#: views/form.py:328
#, python-format
msgid "%s is already an editor"
msgstr ""

#: views/form.py:332
msgid "New editor added ok"
msgstr ""

#: views/form.py:333
#, python-format
msgid "Added editor %s"
msgstr ""

#: views/form.py:344
#, python-format
msgid "Removed editor %s"
msgstr ""

#: views/form.py:369
msgid "Date-time is not valid"
msgstr ""

#: views/form.py:375
#, python-format
msgid "Expiry date set to: %s"
msgstr ""

#: views/form.py:381
msgid "Expiry date cancelled"
msgstr ""

#: views/form.py:383
msgid "Missing date or time"
msgstr ""

#: views/form.py:435
msgid "You can edit the duplicate now"
msgstr ""

#: views/form.py:459
#, python-format
msgid "Public set to: %s"
msgstr ""

#: views/form.py:470
#, python-format
msgid "Shared entries set to: %s"
msgstr ""

#: views/form.py:481
#, python-format
msgid "Restricted access set to: %s"
msgstr ""

#: views/form.py:502
#, python-format
msgid "Data protection consent set to: %s"
msgstr ""

#: views/form.py:513
#, python-format
msgid "Send Confirmation set to: %s"
msgstr ""

#: views/form.py:549
msgid "That form has expired"
msgstr ""

#: views/form.py:551
msgid "That form is not public"
msgstr ""

#: views/site.py:45
msgid "Text saved OK"
msgstr ""

#: views/site.py:57
msgid "Couldn't find that token"
msgstr ""

#: views/site.py:60 views/user.py:199
msgid "Your petition has expired"
msgstr ""

#: views/site.py:65
msgid "Your account has been blocked"
msgstr ""

#: views/site.py:87
msgid "We may have sent you an email"
msgstr ""

#: views/site.py:133
msgid "Site name changed OK"
msgstr ""

#: views/site.py:145 views/user.py:153
msgid "Language updated OK"
msgstr ""

#: views/site.py:156
msgid "Required file is missing"
msgstr ""

#: views/site.py:164
msgid "Bad file name. PNG only"
msgstr ""

#: views/site.py:166
msgid "Favicon changed OK. Refresh with  &lt;F5&gt;"
msgstr ""

#: views/site.py:175
msgid "Favicon reset OK. Refresh with  &lt;F5&gt;"
msgstr ""

#: views/site.py:198
msgid "Confguration saved OK"
msgstr ""

#: views/site.py:209
msgid "SMTP config works!"
msgstr ""

#: views/site.py:230
msgid "Color changed OK"
msgstr ""

#: views/site.py:269
msgid "Site not found"
msgstr ""

#: views/site.py:274
msgid "Cannot delete current site"
msgstr ""

#: views/site.py:277
#, python-format
msgid "Deleted %s"
msgstr ""

#: views/site.py:280
msgid "Site name does not match"
msgstr ""

#: views/site.py:294
#, python-format
msgid "We've sent an invitation to %s"
msgstr ""

#: views/site.py:307
msgid "Opps! We can't find that invitation"
msgstr ""

#: views/site.py:319
msgid "Root disabled"
msgstr ""

#: views/site.py:322
msgid "Root enabled"
msgstr ""

#: views/user.py:54
msgid "Invitation not found"
msgstr ""

#: views/user.py:57
msgid "This invitation has expired"
msgstr ""

#: views/user.py:93
msgid "Opps! An error ocurred when creating the user"
msgstr ""

#: views/user.py:104
msgid "Welcome!"
msgstr ""

#: views/user.py:139 views/user.py:165
#, python-format
msgid "We've sent an email to %s"
msgstr ""

#: views/user.py:177
msgid "Password changed OK"
msgstr ""

#: views/user.py:196
msgid "We couldn't find that petition"
msgstr ""

#: views/user.py:211
msgid "Your email address is valid"
msgstr ""

#: views/user.py:241
msgid "Bad credentials"
msgstr ""

