from api import db
from . import DictMixIn
    
class Invite(db.Model, DictMixIn):
    __tablename__ = "Invites"

    id = db.Column(db.Integer, primary_key=True, index=True)
    createdAt = db.Column(db.DateTime)
    createdBy = db.Column(db.Integer, db.ForeignKey('Users.id'))
    message = db.Column(db.String)
    email = db.Column(db.String, unique=True)
    token = db.Column(db.String)
    # TODO consider use enumtables
    ## comma separated Strings are valid ie: admin, user, owner
    group = db.Column(db.String)

    def __init__(self, email, group, token, created_by, message):
        self.email = email
        self.group = group
        self.token = token
        self.message = message
        self.createdAt = datetime.datetime.now().isoformat()
        self.createdBy = created_by
