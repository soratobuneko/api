import datetime
from sqlalchemy.dialects.postgresql import JSON
from api import db
from . import DictMixIn

class Form(db.Model, DictMixIn):
    __tablename__ = "Forms"

    id = db.Column(db.Integer, primary_key=True, index=True)
    createdAt = db.Column(db.DateTime)
    updatedAt = db.Column(db.DateTime)
    createdBy = db.Column(db.Integer, db.ForeignKey('Users.id'))

    def __init__(self, created_by):
        self.createdAt = datetime.datetime.now().isoformat()
        self.createdBy = created_by
