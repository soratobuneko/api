import datetime
from sqlalchemy.dialects.postgresql import JSON
from werkzeug.security import (generate_password_hash,
    check_password_hash)

from api import db
from . import DictMixIn
    
class User(db.Model, DictMixIn):
    __tablename__ = "Users"

    id = db.Column(db.Integer, primary_key=True, index=True)
    createdAt = db.Column(db.DateTime)
    username = db.Column(db.String, unique=True)
    email = db.Column(db.String, unique=True)
    password_hash = db.Column(db.String)
    # TODO consider use enumtables
    ## comma separated Strings are valid ie: admin, user, owner
    group = db.Column(db.String)

    def __init__(self, username, email, group):
        self.username = username
        self.email = email
        self.group = group
        self.createdAt = datetime.datetime.now().isoformat()

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)
  
    def verify_password(self, password): 
        return check_password_hash(self.password_hash, password)
