FROM python:3.7-slim

RUN mkdir /app
WORKDIR /app

COPY requirements.txt /app
RUN pip install --no-cache-dir -r requirements.txt

COPY . /app
EXPOSE 5000

CMD ["gunicorn", "--bind", ":5000", "--workers", "3", "liberaforms:app"]
