import os
from api import create_app, db
from flask_migrate import Migrate

app = create_app(os.getenv('ENVIRONMENT') or 'default')
migrate = Migrate(app, db)


@app.shell_context_processor
def make_shell_context():
    return dict(app=app, db=db)
